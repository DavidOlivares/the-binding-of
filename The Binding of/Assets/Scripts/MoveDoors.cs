using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDoors : MonoBehaviour
{
    public GameObject door1, door2;
    private Vector3 pos1, pos2;
    public bool open;
    private void Start()
    {
        open = false;
        pos1 = door1.transform.position;
        pos2 = door2.transform.position;

        pos1.x = pos1.z - 5f;
        pos2.x = pos2.z + 0.5f;
    }

    private void Update()
    {
        if(open) OpenDoor();
    }

    public void OpenDoor()
    {
        door1.transform.position = Vector3.MoveTowards(door1.transform.position, pos1, 2*Time.deltaTime);
        door2.transform.position = Vector3.MoveTowards(door2.transform.position, pos2, 2*Time.deltaTime);
    }
}
