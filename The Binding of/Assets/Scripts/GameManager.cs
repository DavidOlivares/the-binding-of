using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//GameManager
public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    public GameObject enemy;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        EventManager.StartGame();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
