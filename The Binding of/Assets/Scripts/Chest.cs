using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chest : MonoBehaviour
{
    private Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetBool("OpenChest", false);
    }

    public void OpenChest()
    {
        animator.SetBool("OpenChest", true);

        // power up etc
    }
}
