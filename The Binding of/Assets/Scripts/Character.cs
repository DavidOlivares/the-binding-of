using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(PlayerInput))]
[RequireComponent(typeof(Animator))]
public class Character : MonoBehaviour
{
    private CharacterController controller;
    private Vector2 direction = Vector2.zero;
    private Vector3 move;
    private bool start = false;

    [SerializeField] private float speed;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float gravity = -9.8f;

    private Animator animator;
    private void Awake()
    {
        EventManager.onStartGame += StartGame;
        EventManager.onStopGame += StopGame;

        /*EventManager.onStartGame -= StartGame;
        EventManager.onStopGame -= StopGame;*/
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("dasdaadadada");
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();    
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("sda");
        if (controller.isGrounded)
        {
                move = new Vector3(direction.x, 0, direction.y);
            move = transform.TransformDirection(move);
            move.z *= speed;

            //transform.Rotate(direction.x * transform.up * rotationSpeed * Time.deltaTime);
        }
        move.y += gravity * Time.deltaTime;
        controller.Move(move * Time.deltaTime);
        animator.SetFloat("movement", direction.y);
    }
    private void Initialize()
    {
    }
    private void StartGame()
    {
        start = true;
    }

    private void StopGame()
    {
        start = false;
    }
    private void OnMove(InputValue value)
    {
        direction = value.Get<Vector2>();
    }
    protected void SetGravity()
    {
        gravity = -9.8f;
    }
}

